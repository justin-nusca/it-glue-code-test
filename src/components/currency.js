import React, {PropTypes} from 'react';

const getNumberAsDollarValue = (value = 0, maxDecimals = 2, groupInterval = 3, groupSeparator = ',') => {
  const splitValues = value.toFixed(maxDecimals).split('.');
  const decimalValue = splitValues[1];

  const dollarValue = splitValues[0].split('').reduceRight((sum, digit, index, digits) => {
    const currIndex = Math.floor(((digits.length - 1) - index) / groupInterval);

    /* eslint-disable no-param-reassign */
    if (!sum[currIndex]) { sum[currIndex] = ''; }
    sum[currIndex] = `${digit}${sum[currIndex]}`;
    /* eslint-enable no-param-reassign */

    return sum;
  }, []).reverse().join(groupSeparator);

  return `$ ${dollarValue}.${decimalValue}`;
};

const Currency = ({value}) => <span>{getNumberAsDollarValue(value)}</span>;

Currency.propTypes = {value: PropTypes.number.isRequired};

export default Currency;
