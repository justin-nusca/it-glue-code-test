import React, {PropTypes} from 'react';

import Currency from '../currency';

import './item.css';

const handleOnChange = (event, onChangeQuantity) => {
  const targetValue = event.target.value;
  const amount = targetValue.length ? parseInt(targetValue, 10) : 1;
  onChangeQuantity(amount);
};

const Item = ({onRemove, onChangeQuantity, price, quantity, title}) => (
  <tr className="item">
    <td>{title}</td>

    <td>
      <input
        className="item__quantity-input"
        id="quantity"
        min="1"
        name="quantity"
        onChange={(event) => { handleOnChange(event, onChangeQuantity); }}
        required
        step="1"
        type="number"
        value={quantity}
      />
    </td>

    <td><Currency value={price} /></td>

    <td>
      <Currency value={price * quantity} />
    </td>
    <td className="item__remove-cntnr">
      <button onClick={onRemove}>×</button>
    </td>
  </tr>
);

Item.propTypes = {
  onRemove: PropTypes.func.isRequired,
  onChangeQuantity: PropTypes.func.isRequired,
  price: PropTypes.number.isRequired,
  quantity: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};

export default Item;
