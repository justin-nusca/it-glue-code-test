import React, {PropTypes} from 'react';

import Currency from '../currency';

import './product.css';

const Product = ({onClick, price, title}) => (
  <li className="product">
    <p className="product__title">{title}</p>
    <p className="product__price"><Currency value={price} /></p>
    <button
      className="product__button"
      onClick={onClick}
    >
      Add To Invoice
    </button>
  </li>
);

Product.propTypes = {
  onClick: PropTypes.func.isRequired,
  price: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};

export default Product;
