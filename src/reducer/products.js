import {handleActions} from 'redux-actions';

import {PRODUCTS} from '../actions/types';

export default handleActions({
  [PRODUCTS.ADD]: (state, {payload: product}) => {
    const existingProduct =
      state.find(stateProduct => stateProduct.title === product.title);

    return existingProduct ? state : [...state, product];
  },
}, []);
