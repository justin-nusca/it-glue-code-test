# Justin Nusca’s Submission Notes

After looking at the text-based mockup, I decided to separate the creation of “products” from adding them to the invoice itself, as it would help ensure a standardized set of products/services that could be added to an invoice,
removing the potential for item name typos, duplicate entries, etc.

In addition to React & Redux …

- I used [redux-actions](https://github.com/acdlite/redux-actions) as an action creator to ensure a standardized shape to dispatched action objects.
- I also added [Jest](https://facebook.github.io/jest/) as the testing library for the reducers & actions.

## TODOs

- I created action types for editing the title or price of existing Products, but didn’t get a chance to actually add the functionality to the app — this was mostly a time constraint around providing a nice enough UX to allow for it.
- I didn’t put much thought into building for anything beyond my own dev environment, and while I did make a minimal effort to ensure the page was reasonably viewable on smaller (ie, mobile) screens, I haven’t added, for example, CSS autoprefixing for backwards compatibility, or adding a properly minified production build option.
- Related to the above, I’m naively relying on HTML5-standard form validation, the implementation of which also varies between browsers. Inputs optimisticly assume valid values, which isn’t guaranteed to be the case for, say, some mobile devices or old browsers. More defensive checking and sanitizing of input values would be needed if I were to keep working on this.
- I haven’t created tests for some of the components, some of which (currency & currencyTotal) includes application logic that doesn’t exist elsewhere (ie, tallying & formatting values for display in the UI). If I were to work on this further, I would probably extract aforementioned logic to an external utility-fns file to allow it to be tested outside of the context of the component.