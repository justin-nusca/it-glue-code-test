import {handleActions} from 'redux-actions';

import {ITEMS} from '../actions/types';

export default handleActions({
  [ITEMS.SET_QUANTITY]: (state, {payload: {productId: target, quantity}}) => {
    if (state.productId !== target) { return state; }

    return Object.assign({}, state, {quantity: Math.max(1, quantity)});
  },
}, {});
