import React, {PropTypes} from 'react';

import './createProductForm.css';

const parsePriceValue = value => (value.length ? parseFloat(value) : 0);

class createProductForm extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {price: '', title: ''};
  }

  onChange(event) {
    const target = event.target;
    const value = target.name === 'price' ? parsePriceValue(target.value) : target.value;

    this.setState({[target.name]: value});
  }

  onSubmit(event) {
    event.preventDefault();
    event.stopPropagation();

    this.props.onSubmit(this.state);
    this.setState({price: '', title: ''});
  }

  render() {
    const {price, title} = this.state;

    return (
      <form className="product-form" id="create-product-form" onSubmit={this.onSubmit}>
        <span className="product-form__cntnr">
          <label className="product-form__label" htmlFor="title">Name</label>
          <input
            className="product-form__input"
            id="title"
            name="title"
            onChange={this.onChange}
            placeholder="Name"
            required
            type="text"
            value={title}
          />
        </span>

        <span className="product-form__cntnr">
          <label className="product-form__label" htmlFor="price">Price</label>
          <input
            className="product-form__input"
            id="price"
            min="0.01"
            name="price"
            onChange={this.onChange}
            placeholder="1.00"
            required
            step="0.01"
            type="number"
            value={price}
          />
        </span>

        <input className="product-form__submit" type="submit" />
      </form>
    );
  }
}

createProductForm.propTypes = {onSubmit: PropTypes.func.isRequired};

export default createProductForm;
