const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const PATHS = {
  BUILD: path.resolve(__dirname, 'dist'),
  SRC: path.resolve(__dirname, 'src'),
};

module.exports = {
  devtool: 'source-map',
  entry: {
    app: path.resolve(PATHS.SRC, 'app.js'),
  },
  output: {
    filename: 'bundle.js',
    path: PATHS.BUILD,
    publicPath: '/',
  },
  module: {
    loaders: [{
      test: /\.css$/,
      include: PATHS.SRC,
      loader: [{
        loader: 'style-loader',
      }, {
        loader: 'css-loader',
      }],
    }, {
      test: /\.html$/,
      loader: 'html-loader',
      query: { interpolate: true, minimize: true },
    }, {
      test: /\.js$/,
      include: PATHS.SRC,
      loader: 'babel-loader',
      query: {
        presets: ['es2015', 'react'],
      },
    }],
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: false,
      inject: 'body',
      template: path.resolve(PATHS.SRC, 'index.html'),
    }),
  ],
};
