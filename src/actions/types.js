export const ITEMS = {
  ADD: 'ADD_ITEM',
  CLEAR: 'CLEAR_ITEMS',
  REMOVE: 'REMOVE_ITEM',
  SET_QUANTITY: 'SET_QUANTITY',
};

export const PRODUCTS = {
  ADD: 'ADD_PRODUCT',
  REMOVE: 'REMOVE_PRODUCT',
  SET_PRICE: 'SET_PRICE',
  SET_TITLE: 'SET_TITLE',
};
