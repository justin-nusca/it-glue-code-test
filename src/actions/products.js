import {createAction} from 'redux-actions';

import {PRODUCTS} from './types';

let productId = 1;

const addProduct = createAction(PRODUCTS.ADD, (product) => {
  const productWithId = Object.assign({}, product, {id: productId});
  productId += 1;
  return productWithId;
});

export default addProduct;
