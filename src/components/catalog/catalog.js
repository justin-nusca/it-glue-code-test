import React, {PropTypes} from 'react';
import {connect} from 'react-redux';

import addProduct from '../../actions/products';
import CreateProductForm from '../createProductForm/createProductForm';
import ProductsList from '../productsList/productsList';
import {addItem} from '../../actions/items';

const catalog = ({onAddItem, onAddProduct, products}) => (
  <div className="catalog">
    <h1 className="title">Catalog</h1>
    <ProductsList onAdd={onAddItem} products={products} />
    <h2 className="subtitle">Add New Product</h2>
    <CreateProductForm onSubmit={onAddProduct} />
  </div>
);

catalog.propTypes = {
  onAddItem: PropTypes.func.isRequired,
  onAddProduct: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default connect(state => ({
  products: state.products,
}), {
  onAddItem: addItem,
  onAddProduct: addProduct,
})(catalog);
