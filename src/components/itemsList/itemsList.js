import React, {PropTypes} from 'react';

import Item from '../item/item';

const renderItems = (items, onRemove, onSetQuantity, products) => (
  items.map((item) => {
    const {productId, quantity} = item;
    const currProduct = products.find(product => productId === product.id);

    return (
      <Item
        key={productId}
        onRemove={() => onRemove(productId)}
        onChangeQuantity={(amount) => { onSetQuantity({productId, quantity: amount}); }}
        price={currProduct.price}
        quantity={quantity}
        title={currProduct.title}
      />
    );
  })
);

export const ItemInvoice = ({items, onRemove, onSetQuantity, products}) => (
  <tbody>{renderItems(items, onRemove, onSetQuantity, products)}</tbody>
);

ItemInvoice.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  onRemove: PropTypes.func.isRequired,
  onSetQuantity: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ItemInvoice;
