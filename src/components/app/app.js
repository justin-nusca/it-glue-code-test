import React, {PropTypes} from 'react';
import {Provider} from 'react-redux';

import Catalog from '../catalog/catalog';
import Invoice from '../invoice/invoice';

import './app.css';

const App = ({store}) => (
  <Provider store={store}>
    <div className="app">
      <Invoice />
      <Catalog />
    </div>
  </Provider>
);

App.propTypes = {store: PropTypes.shape({
  dispatch: PropTypes.func.isRequired,
  getState: PropTypes.func.isRequired,
  replaceReducer: PropTypes.func.isRequired,
  subscribe: PropTypes.func.isRequired,
}).isRequired};

export default App;
