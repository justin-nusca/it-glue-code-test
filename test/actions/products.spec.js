import addProduct from '../../src/actions/products';
import {PRODUCTS} from '../../src/actions/types';
import {cog, widget} from '../../src/data/products';

describe('Products Actions', () => {
  it('should create an action to add a product', () => {
    const expectedAction = {type: PRODUCTS.ADD, payload: widget};

    expect(addProduct(widget)).toMatchObject(expectedAction);
  });

  it('should create an id for the product being added', () => {
    const addWidgetAction = addProduct(widget);
    expect(addWidgetAction.payload.id).toBeDefined();

    const addCogAction = addProduct(cog);
    expect(addCogAction.payload.id).toBeDefined();

    expect(addCogAction.payload.id).not.toBe(addWidgetAction.payload.id);
  });
});
