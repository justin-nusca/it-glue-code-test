import {handleActions} from 'redux-actions';

import item from './item';
import {ITEMS} from '../actions/types';

export default handleActions({
  [ITEMS.CLEAR]: () => [],
  [ITEMS.ADD]: (state, {payload: id}) => {
    const existingItem = state.find(stateItem => stateItem.productId === id);

    return existingItem ? state : [...state, {productId: id, quantity: 1}];
  },
  [ITEMS.REMOVE]: (state, {payload: id}) => {
    const itemIndex = state.findIndex(stateItem => stateItem.productId === id);

    return itemIndex >= 0 ?
      [...state.slice(0, itemIndex), ...state.slice(itemIndex + 1)] : state;
  },
  [ITEMS.SET_QUANTITY]: (state, action) => (
    state.map(stateItem => item(stateItem, action))
  ),
}, []);
