import {createStore, compose} from 'redux';

import reducer from '../reducer';

export default initialState => compose(createStore)(reducer, initialState);
