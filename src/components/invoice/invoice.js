import React, {PropTypes} from 'react';
import {connect} from 'react-redux';

import InvoiceTotal from '../invoiceTotal/invoiceTotal';
import ItemsList from '../itemsList/itemsList';
import {removeItem, setItemQuantity} from '../../actions/items';

import './invoice.css';

const renderInvoiceContents = (items, onRemove, onSetQuantity, products) => (
  items.length ? (
    <div className="invoice__table-cntnr">
      <table className="invoice__table">
        <thead className="invoice__labels">
          <tr>
            <th>Item</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Total</th>
          </tr>
        </thead>

        <ItemsList
          items={items}
          products={products}
          onRemove={onRemove}
          onSetQuantity={onSetQuantity}
        />
      </table>
    </div>
  ) : (
    <div className="invoice--empty">
      <p>You have not added any products to your order.</p>
    </div>
  )
);

export const invoice = ({items, onRemove, onSetQuantity, products}) => (
  <div className="invoice">
    <h1 className="title">Invoice</h1>
    {renderInvoiceContents(items, onRemove, onSetQuantity, products)}
    <InvoiceTotal invoice={items} products={products} />
  </div>
);

invoice.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  onRemove: PropTypes.func.isRequired,
  onSetQuantity: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default connect(state => state, {
  onRemove: removeItem,
  onSetQuantity: setItemQuantity,
})(invoice);
