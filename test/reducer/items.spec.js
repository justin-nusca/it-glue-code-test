import items from '../../src/reducer/items';
import {ITEMS} from '../../src/actions/types';

describe('Items Reducer', () => {
  it('has a default state', () => {
    expect(items(undefined, {type: 'unknown'})).toEqual([]);
  });

  describe('when CLEAR_ITEMS action is called', () => {
    it('empties the items', () => {
      const initialState = [{productId: 1, quantity: 1}];
      const state = items(initialState, {type: ITEMS.CLEAR});

      expect(state).toHaveLength(0);
    });
  });

  describe('when CLEAR_ITEMS action is called', () => {
    it('empties the items', () => {
      const initialState = [{productId: 1, quantity: 1}];
      const state = items(initialState, {type: ITEMS.CLEAR});

      expect(state).toHaveLength(0);
    });
  });

  describe('when ADD_ITEM action is called', () => {
    it('adds an item', () => {
      const productId = 1;
      const state = items([], {type: ITEMS.ADD, payload: productId});

      expect(state).toHaveLength(1);
      expect(state).toEqual([{quantity: 1, productId}]);
    });

    it('will add only one of a given item', () => {
      const initialState = [{productId: 1, quantity: 1}];
      const state = items(initialState, {type: ITEMS.ADD, payload: 1});
      expect(state).toHaveLength(1);
    });
  });

  describe('when REMOVE_ITEM action is called', () => {
    it('will remove the item from the items', () => {
      const initialState = [{productId: 1}, {productId: 2}, {productId: 3}];
      const state = items(initialState, {type: ITEMS.REMOVE, payload: 2});

      expect(state).toHaveLength(initialState.length - 1);
      expect(state[1].productId).not.toBe(2);
    });

    it('will do nothing when attemping to remove items not in the items', () => {
      const initialState = [{productId: 1}, {productId: 2}, {productId: 3}];
      const state = items(initialState, {type: ITEMS.REMOVE, payload: 5});

      expect(state).toHaveLength(initialState.length);
      expect(state).toEqual(initialState);
    });
  });
});
