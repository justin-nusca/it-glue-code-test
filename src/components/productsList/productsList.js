import React, {PropTypes} from 'react';

import Product from '../product/product';

import './productsList.css';

const renderProducts = (onAdd, products) => (
  products.map(product => (
    <Product
      key={product.id}
      onClick={() => onAdd(product.id)}
      price={product.price}
      title={product.title}
    />
  ))
);

const ProductsList = ({onAdd, products}) => (
  <ul className="products-list">{renderProducts(onAdd, products)}</ul>
);

ProductsList.propTypes = {
  onAdd: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ProductsList;
