import * as actions from '../../src/actions/items';
import {ITEMS} from '../../src/actions/types';

describe('Items Actions', () => {
  it('should create an action to add an item', () => {
    const payload = 1;
    const expectedAction = {type: ITEMS.ADD, payload};

    expect(actions.addItem(payload)).toEqual(expectedAction);
  });

  it('should create an action to clear all items', () => {
    const expectedAction = {type: ITEMS.CLEAR};

    expect(actions.clearItems()).toEqual(expectedAction);
  });

  it('should create an action to remove an item', () => {
    const payload = 1;
    const expectedAction = {type: ITEMS.REMOVE, payload};

    expect(actions.removeItem(payload)).toEqual(expectedAction);
  });

  it('should create an action to set the quantity of an item', () => {
    const payload = {id: 1, quantity: 5};
    const expectedAction = {type: ITEMS.SET_QUANTITY, payload};

    expect(actions.setItemQuantity(payload)).toEqual(expectedAction);
  });
});
