import {widget} from '../../src/data/products';
import products from '../../src/reducer/products';
import {PRODUCTS} from '../../src/actions/types';

describe('Products Reducer', () => {
  it('has a default state', () => {
    expect(products(undefined, {type: 'unknown'})).toEqual([]);
  });

  describe('when ADD_PRODUCT action is called', () => {
    it('adds an item', () => {
      const state = products([], {type: PRODUCTS.ADD, payload: widget});

      expect(state).toHaveLength(1);
      expect(state).toEqual([widget]);
    });

    it('will add only one of a given product title', () => {
      const initialState = [widget];
      const state = products(initialState, {type: PRODUCTS.ADD, payload: widget});
      expect(state).toHaveLength(1);
    });
  });
});
