import React, {PropTypes} from 'react';

import Currency from '../currency';

import './invoiceTotal.css';

const getTotal = (invoice, products) => (
  invoice.reduce((sum, {productId, quantity}) => {
    const price = products.find(product => product.id === productId).price;
    return sum + (price * quantity);
  }, 0)
);

const getTaxAsPercentage = tax => `${tax * 100}%`;

const TAX_AMOUNT = 0.05;

export const invoiceTotal = ({invoice, products}) => {
  const subTotal = getTotal(invoice, products);
  const taxPercentage = getTaxAsPercentage(TAX_AMOUNT);
  const taxTotal = subTotal * TAX_AMOUNT;

  return (
    <div className="invoice-total">
      <p>
        <span className="invoice-total__label">Subtotal</span>
        <Currency value={subTotal} />
      </p>

      <p>
        <span className="invoice-total__label">{`Tax (${taxPercentage})`}</span>
        <Currency value={taxTotal} />
      </p>

      <p className="invoice-total__total">
        <span className="invoice-total__label">Total</span>
        <Currency value={subTotal + taxTotal} />
      </p>
    </div>
  );
};

invoiceTotal.propTypes = {
  invoice: PropTypes.arrayOf(PropTypes.object).isRequired,
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default invoiceTotal;
