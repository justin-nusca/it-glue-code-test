import item from '../../src/reducer/item';
import {ITEMS} from '../../src/actions/types';

describe('Item Reducer', () => {
  it('has a default state', () => {
    expect(item(undefined, {type: 'unknown'})).toBeDefined();
  });

  describe('when SET_QUANTITY action is called', () => {
    it('sets the quantity of the item to the given value', () => {
      const initialState = {productId: 1, quantity: 1};
      const amount = 5;
      const state = item(initialState, {
        type: ITEMS.SET_QUANTITY,
        payload: {productId: 1, quantity: amount},
      });

      expect(state.quantity).toBe(amount);
    });

    it('does not allow the quantity to be below 1', () => {
      const initialState = {productId: 1, quantity: 1};
      const amount = -1;
      const state = item(initialState, {
        type: ITEMS.SET_QUANTITY,
        payload: {productId: 1, quantity: amount},
      });

      expect(state.quantity).toBe(1);
    });
  });
});
