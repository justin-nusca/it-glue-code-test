export const cog = {
  price: 15.99,
  title: 'Cog',
};

export const doodad = {
  price: 99.99,
  title: 'Doodad',
};

export const gadget = {
  price: 55.75,
  title: 'Gadget',
};

export const gizmo = {
  price: 3.33,
  title: 'Gizmo',
};

export const widget = {
  price: 10,
  title: 'Widget',
};

const productData = [cog, doodad, gadget, gizmo, widget];

export default productData;
