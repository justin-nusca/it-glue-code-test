import {createAction} from 'redux-actions';

import {ITEMS} from './types';

export const addItem = createAction(ITEMS.ADD);
export const clearItems = createAction(ITEMS.CLEAR);
export const removeItem = createAction(ITEMS.REMOVE);
export const setItemQuantity = createAction(ITEMS.SET_QUANTITY);
