/* eslint-env browser */
import React from 'react';
import {render} from 'react-dom';

import addProduct from './actions/products';
import productData from './data/products';
import App from './components/app/app';
import createStore from './store';

import './global.css';

const store = createStore({});

productData.forEach(productDatum => store.dispatch(addProduct(productDatum)));

render(<App store={store} />, document.getElementById('app'));
